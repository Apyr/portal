interface LoginArgs {
  username: string;
  password: string;
}

export default {
  namespaced: true,
  state: {
    authorized: false
  },
  mutations: {
    LOGIN(state: any) {
      state.authorized = true;
    },
    LOGOUT(state: any) {
      state.authorized = false;
    }
  },
  actions: {
    async login({ commit, dispatch }: any, { username, password }: LoginArgs) {
      if (username === 'admin' && password === 'admin') {
        commit('LOGIN');
      } else {
        dispatch('showError', 'Неправильный логин или пароль', { root: true });
      }
    }
  }
};