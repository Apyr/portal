from rest_framework import serializers
from django.contrib.auth import get_user_model
from captcha_app import CaptchaSerializer

from . import models


User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'is_superuser', 'last_login', 'date_joined')


class TagsField(serializers.SlugRelatedField):
    
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        try:
            v, _ = self.get_queryset().get_or_create(**{self.slug_field: data})
            return v
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=smart_text(data))
        except (TypeError, ValueError):
            self.fail('invalid')


class PostSerializer(CaptchaSerializer, serializers.ModelSerializer):
    tags = TagsField(
        many=True,
        read_only=False,
        slug_field='name',
        queryset = models.Tag.objects.all()
    )
    user = UserSerializer(read_only=True)

    class Meta:
        model = models.Post
        fields = '__all__'
        read_only_fields = ('updated_at', 'created_at')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tag
