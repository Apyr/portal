from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Post(models.Model):
    PRIVATE = 'private'
    PUBLIC = 'public'

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, default='')
    text = models.TextField(default='')
    preview_text = models.TextField(default='')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField('Tag')
    protection = models.CharField(max_length=16, default=PRIVATE, choices=((PRIVATE, PRIVATE), (PUBLIC, PUBLIC)))

    def __str__(self):
        return f'{self.id} {self.title} ({self.user.username})'


class Tag(models.Model):
    name = models.CharField(max_length=255, default='', unique=True)

    def __str__(self):
        return self.name
