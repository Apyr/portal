from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework import permissions, filters
from django_filters.rest_framework import DjangoFilterBackend

from . import models, serializers


class PostViewSet(ModelViewSet):
    queryset = models.Post.objects.all().prefetch_related('tags').order_by('-id')
    serializer_class = serializers.PostSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.DjangoObjectPermissions)

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.is_superuser:
            qs = qs.filter(user__id=self.request.user.id)
        return qs

    def filter_queryset(self, queryset):
        params = self.request.query_params
        if 'tags' in params:
            tags = params.getlist('tags')
            if tags:
                queryset = queryset.filter(tags__name__in=tags)
        return super().filter_queryset(queryset)

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)


class TagViewSet(ReadOnlyModelViewSet):
    queryset = models.Tag.objects.all()
    serializer_class = serializers.TagSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (filters.SearchFilter,) #/tags/?search=something
    search_fields = ('name',)
