from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('jet/', include('jet.urls', 'jet')),

    path('api/diary/', include('diary.urls')),
    path('api/captcha/', include('captcha_app.urls')),
    path('api/', include('my_auth.urls')),
]
