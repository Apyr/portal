from django.contrib.auth import get_user_model
from rest_framework import serializers

from .. import models


User = get_user_model()



class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Profile


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        exclude = ('password',)
