from django.contrib.auth import get_user_model
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated

from .. import serializers


User = get_user_model()


class UserViewSet(ReadOnlyModelViewSet):
    queryset = User.objects.all().select_related('profile').order_by('id')
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAuthenticated,)
