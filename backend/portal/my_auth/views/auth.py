from django.contrib.auth import login, logout, get_user_model
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import IsAuthenticated

from .. import serializers


User = get_user_model()


@api_view(['POST'])
def login_handler(request):
    s = serializers.LoginSerializer(data=request.data)
    s.is_valid(raise_exception=True)
    data = s.validated_data
    user = User.objects.filter(username=data['username']).select_related('profile').first()
    if user is None or not user.check_password(data['password']):
        raise AuthenticationFailed()
    login(request, user)
    return Response(serializers.UserSerializer(user).data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def logout_handler(request):
    logout(request)
    return Response({'result': 'ok'})


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def change_password(request):
    s = serializers.ChangePasswordSerializer(data=request.data)
    s.is_valid(raise_exception=True)
    data = s.validated_data
    if not request.user.check_password(data['old_password']):
        raise AuthenticationFailed()
    request.user.set_password(data['new_password'])
    request.user.save()
    return Response({'result': 'ok'})


@api_view(['POST'])
def register(request):
    s = serializers.RegisterSerializer(data=request.data)
    s.is_valid(raise_exception=True)
    data = s.validated_data
    user = User.objects.create_user(data['username'], data['email'], data['password'])
    return Response(serializers.UserSerializer(user).data)


@ensure_csrf_cookie
@api_view(['GET'])
def get_current_user(request):
    user = None
    if hasattr(request, 'user'):
        user = request.user
    if user is None:
        return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        return Response(serializers.UserSerializer(user).data)
