from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register('users', views.UserViewSet)


auth_urls = [
    path('login/', views.login_handler),
    path('logout/', views.logout_handler),
    path('register/', views.register),
    path('password/change/', views.change_password),
    path('user/', views.get_current_user),
]


urlpatterns = [
    path('auth/', include(auth_urls)),
    path('', include(router.urls)),
]
