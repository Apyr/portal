from django.urls import path, include

from . import views


urlpatterns = [
    path('image/<key>/', views.get_image),
    path('new/', views.create),
]
