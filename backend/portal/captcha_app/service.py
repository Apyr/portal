import io
import string
import random
from datetime import datetime
from time import sleep
from threading import Lock, Thread
from captcha.image import ImageCaptcha


generator = ImageCaptcha(width=240, height=80)

CHARS = string.ascii_uppercase + string.digits


def generate(length):
    seq = (random.choice(CHARS) for _ in range(length))
    return ''.join(seq)


class Captcha:
    
    def __init__(self):
        self.text = generate(8)
        self.key = generate(32)
        self.image = generator.generate(self.text).getvalue()
        self.date = datetime.now()

    @property
    def lived(self):
        now = datetime.now()
        return (now - self.date).total_seconds()


THREAD_TIMEOUT = 60


class Service:
    
    TIMEOUT = 300

    def __init__(self):
        self.__lock = Lock()
        self.__data = {}
        thread = Thread(name='captcha-service', target=self.__run)
        thread.daemon = True
        thread.start()

    def __clean(self):
        with self.__lock:
            vals = tuple(self.__data.values())
            for c in vals:
                if c.lived > self.TIMEOUT:
                    del self.__data[c.key]

    def __run(self):
        while True:
            sleep(THREAD_TIMEOUT)
            self.__clean()

    def get(self, key):
        with self.__lock:
            return self.__data.get(key)

    def create(self):
        c = Captcha()
        with self.__lock:
            self.__data[c.key] = c
        return c

    def solve(self, key, solution):
        solution = solution.strip().upper()
        with self.__lock:
            c = self.__data.get(key)
            if c is not None and c.text == solution and c.lived < self.TIMEOUT:
                del self.__data[key]
                return True
        return False


service = Service()


__all__ = ('service',)
