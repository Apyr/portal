from rest_framework import status, exceptions
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import BaseRenderer

from .service import service


class BinaryRenderer(BaseRenderer):
    media_type = 'image/png'
    format = None
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data


@api_view(['GET'])
@renderer_classes([BinaryRenderer])
def get_image(request, key=None):
    captcha = service.get(key)
    if captcha is None:
        raise exceptions.NotFound()
    return Response(captcha.image, content_type='image/png')


@api_view(['POST'])
def create(request):
    captcha = service.create()
    return Response({'key': captcha.key})
