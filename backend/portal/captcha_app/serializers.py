from rest_framework import serializers
from .service import service


class CaptchaFieldSerializer(serializers.Serializer):
    key = serializers.CharField(max_length=64)
    value = serializers.CharField(max_length=64)


class CaptchaSerializer(serializers.Serializer):
    captcha = CaptchaFieldSerializer(write_only=True)

    def validate(self, data):
        c = data.pop('captcha', None)
        result = service.solve(c['key'], c['value'])
        if not result:
            raise serializers.ValidationError('Invalid captcha')
        return super().validate(data)
