from django.apps import AppConfig


class CaptchaAppConfig(AppConfig):
    name = 'Captcha App'
