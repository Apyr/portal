#!/bin/bash
set -e

echo "Migration..."
./manage.py migrate
echo "Migrated"

echo "Running"

#should replace "--http" to "--socket"
uwsgi --http 0.0.0.0:8000 --module portal.wsgi
