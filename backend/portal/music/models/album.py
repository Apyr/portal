from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Album(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(default='')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='albums')
    cover_image = models.ImageField(blank=True)
    
    class Meta:
        unique_together = ('name', 'user')

    def __str__(self):
        return f'№{self.id} {self.name} ({self.user.username})'
