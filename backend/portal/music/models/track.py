from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Track(models.Model):
    author = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    length = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tracks')
    albums = models.ManyToManyField('Album')
    cover_image = models.ImageField(blank=True)
    audio_file = models.FileField(blank=True)

    class Meta:
        unique_together = ('author', 'title', 'user')
    
    def __str__(self):
        return f'№{self.id} {self.author} - {self.title} ({self.user.username})'
