from django.contrib import admin

from . import models


class TrackAdmin(admin.ModelAdmin):
    pass


class AlbumAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Track, TrackAdmin)
admin.site.register(models.Album, AlbumAdmin)
